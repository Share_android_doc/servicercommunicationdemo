package com.example.kimsoerhrd.servercommunicationdemo;

import android.support.v7.app.AppCompatActivity;

public class AsyncTask extends android.os.AsyncTask<String, Integer, Boolean> {
    AppCompatActivity appCompatActivity;
    DialogProgressBar dialogProgressBar;

    public AsyncTask(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialogProgressBar= new DialogProgressBar();
        dialogProgressBar.show(appCompatActivity.getSupportFragmentManager(),"ProgressBar");
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        for (int i=0; i<100; i++){
            publishProgress(i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        dialogProgressBar.updateProgressValue(values[0]);
        dialogProgressBar.updateProgressResult(values[0]+"%");
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        dialogProgressBar.updateProgressResult("Complete");
    }
}
