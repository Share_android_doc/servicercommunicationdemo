package com.example.kimsoerhrd.servercommunicationdemo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;


public class DialogProgressBar  extends DialogFragment {
  ProgressBar progressBar;
  TextView progressResult;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("downloading")
                .setNegativeButton("Cancell", null);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.progressbar_layout, null);
        progressBar = view.findViewById(R.id.progressBar);
        progressResult =view.findViewById(R.id.tvProgressValue);
        builder.setView(view);

        return builder.create();
    }

    void updateProgressValue(int value){
        if (progressBar !=null)
            progressBar.setProgress(value);
    }

    void updateProgressResult(String value){
        if (progressResult !=null){
            progressResult.setText(value);
        }
    }
}
